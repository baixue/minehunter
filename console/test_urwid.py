#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urwid


def exit_on_q(key):
    if key in ('q', 'Q'):
        raise urwid.ExitMainLoop()


class QuestionBox(urwid.Filler):
    def keypress(self, size, key):
        if key == 'enter':
            pass
        elif key == ' ':
            pass
        elif key in ('left', 'right', 'up', 'down'):
            self.original_widget.set_edit_text(repr(key))
        elif key in ('q', 'Q'):
            raise urwid.ExitMainLoop()
        else:
            return super(QuestionBox, self).keypress(size, key)


edit = urwid.Edit(u'', align='center')
fill = QuestionBox(edit)
loop = urwid.MainLoop(fill)
loop.run()
