#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urwid


def key_press(key):
    if key in ('q', 'Q'):
        raise urwid.ExitMainLoop()
    elif key == 'enter':
        txt.set_text(u'XXXXXXXXXXXXXX')
    elif key == ' ':
        pass
    else:
        txt.set_text(repr(key))

txt = urwid.Text(u'* * * * * * * *\n\n* * * * * * * *\n\n* * * * * * * *\n', align='center')
fill = urwid.Filler(txt, 'middle')
loop = urwid.MainLoop(fill, unhandled_input=key_press)
loop.run()
