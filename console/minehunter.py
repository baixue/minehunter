# -*- coding: utf-8 -*-
import time
import random
import urwid


PALETTE = [
    ('normal', 'gray', 'default'),
    ('active', 'green', 'default'),
    ('flag', 'red', 'default'),
    ('bomb', 'light red', 'default', 'bold'),
]


class Element(object):
    status_view = {'hided': '*', 'flag': 'X', 'bomb': '@', 'show': ''}

    def __init__(self, status='hided'):
        self.status = status
        self.clicked = False
        self.has_flag = False
        self.bomb = False
        self.hint = -1

    def view(self):
        return self.status_view[self.status]


class Board(object):
    num_cols = 12
    num_rows = 12

    def __init__(self):
        self.remaining = 0
        self.mines = 0
        self.flags = 0
        self.playing = True
        self.won = False
        self.elements = []

        for i in xrange(self.num_cols * self.num_rows):
            self.elements.append(Element())
        self.reset()

    def on_board(self, x, y):
        return all([x >= 0, y < self.num_rows, y >= 0, y < self.num_cols])

    def _element(self, x, y):
        if self.on_board(x, y):
            return self.elements[y + self.num_rows * x]

    def draw(self):
        pass

    def show(self):
        pass

    def reset(self):
        pass

    def click(self, x, y):
        if not self.playing:
            return False

        e = self._element(x, y)
        if e is None or e.clicked:
            return False


display = urwid.Text(u"", align="center")
fill = urwid.Filler(display, 'middle')
loop = urwid.MainLoop(fill, palette, unhandled_input=key_press)
loop.run()
